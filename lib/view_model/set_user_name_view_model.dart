import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SetUserNameViewModel {
  final _userNameController = BehaviorSubject<String>();

  Stream<String> get outputUserName =>
      _userNameController.stream.map((event) => event);

  Sink<String> get inputUserName => _userNameController.sink;

  getUserName() async {
    final prefs = await SharedPreferences.getInstance();
    final String userName = prefs.getString('userName') ?? '';
    inputUserName.add(userName);
  }

  setUserName(String userName) async{
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('userName', userName);
    getUserName();
  }

  removeUsername() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove('userName');
  } 
}

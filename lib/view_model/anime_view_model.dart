import 'package:flutter_mvvm/model/anime.dart';
import 'package:flutter_mvvm/model/anime_response.dart';
import 'package:flutter_mvvm/repository/anime_repository.dart';
import 'package:rxdart/subjects.dart';

import '../data/network/api_response.dart';

class AnimeViewModel {
  AnimeRepository _animeRepository = AnimeRepository();

  final BehaviorSubject<ApiResponse<AnimeResponse>>
      _animeListFromApiController =
      BehaviorSubject<ApiResponse<AnimeResponse>>();

  Sink<ApiResponse<AnimeResponse>> get inputAnimeListFromApi =>
      _animeListFromApiController.sink;

  Stream<ApiResponse<AnimeResponse>> get outputAnimeListFromApi =>
      _animeListFromApiController.stream.map((event) => event);

  final BehaviorSubject<List<Anime>> _animeListFromDbController =
      BehaviorSubject<List<Anime>>();

  Sink<List<Anime>> get inputAnimeListFromDb => _animeListFromDbController.sink;

  Stream<List<Anime>> get outputAnimeListFromDb =>
      _animeListFromDbController.stream.map((event) => event);

  fetchAnimeListFromApi() {
    inputAnimeListFromApi.add(ApiResponse.loading());
    _animeRepository
        .getAnimeListFromApi()
        .then(
            (value) => inputAnimeListFromApi.add(ApiResponse.completed(value)))
        .onError((error, stackTrace) =>
            inputAnimeListFromApi.add(ApiResponse.error(error.toString())));
  }

  fetchAnimeListFromDb() {
    _animeRepository
        .getAnimeListFromDb()
        .then((value) => inputAnimeListFromDb.add(value));
  }

  addAnimeListToApi(Anime anime) {
    _animeRepository.insertAnimeToDb(anime);
  }
}

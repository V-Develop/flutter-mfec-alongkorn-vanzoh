import 'package:flutter/material.dart';
import 'package:flutter_mvvm/view/screen/anime_list_screen.dart';
import 'package:flutter_mvvm/view/screen/favorite_anime_list_screen.dart';
import 'package:flutter_mvvm/view/screen/set_user_name_screen.dart';
import 'package:flutter_mvvm/view/screen/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const FavoriteAnimeListScreen()));
              },
              child: const Icon(Icons.favorite),
            )
          ],
        ),
        body:
            const SplashScreen() // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}

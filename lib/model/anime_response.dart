import 'dart:convert';

import 'anime.dart';
/// pagination : {"last_visible_page":20,"has_next_page":true}
/// data : [{"mal_id":1,"name":"Pierrot","url":"https://myanimelist.net/anime/producer/1/Pierrot","count":283},null]

AnimeResponse animeResponseFromJson(String str) => AnimeResponse.fromJson(json.decode(str));
String animeResponseToJson(AnimeResponse data) => json.encode(data.toJson());
class AnimeResponse {
  AnimeResponse({
      Pagination? pagination, 
      List<Anime>? data,}){
    _pagination = pagination;
    _data = data;
}

  AnimeResponse.fromJson(dynamic json) {
    _pagination = json['pagination'] != null ? Pagination.fromJson(json['pagination']) : null;
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Anime.fromJson(v));
      });
    }
  }
  Pagination? _pagination;
  List<Anime>? _data;
AnimeResponse copyWith({  Pagination? pagination,
  List<Anime>? data,
}) => AnimeResponse(  pagination: pagination ?? _pagination,
  data: data ?? _data,
);
  Pagination? get pagination => _pagination;
  List<Anime>? get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_pagination != null) {
      map['pagination'] = _pagination?.toJson();
    }
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// last_visible_page : 20
/// has_next_page : true

Pagination paginationFromJson(String str) => Pagination.fromJson(json.decode(str));
String paginationToJson(Pagination data) => json.encode(data.toJson());
class Pagination {
  Pagination({
      int? lastVisiblePage, 
      bool? hasNextPage,}){
    _lastVisiblePage = lastVisiblePage;
    _hasNextPage = hasNextPage;
}

  Pagination.fromJson(dynamic json) {
    _lastVisiblePage = json['last_visible_page'];
    _hasNextPage = json['has_next_page'];
  }
  int? _lastVisiblePage;
  bool? _hasNextPage;
Pagination copyWith({  int? lastVisiblePage,
  bool? hasNextPage,
}) => Pagination(  lastVisiblePage: lastVisiblePage ?? _lastVisiblePage,
  hasNextPage: hasNextPage ?? _hasNextPage,
);
  int? get lastVisiblePage => _lastVisiblePage;
  bool? get hasNextPage => _hasNextPage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['last_visible_page'] = _lastVisiblePage;
    map['has_next_page'] = _hasNextPage;
    return map;
  }

}
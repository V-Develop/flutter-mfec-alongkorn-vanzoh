import 'dart:convert';

/// mal_id : 1
/// name : "Pierrot"
/// url : "https://myanimelist.net/anime/producer/1/Pierrot"
/// count : 283
const String tableAnime = 'anime';
const String columnId = 'mal_id';
const String columnName = 'name';
const String columnUrl = 'url';
const String columnCount = 'count';

Anime dataFromJson(String str) => Anime.fromJson(json.decode(str));

String dataToJson(Anime data) => json.encode(data.toJson());

class Anime {
  Anime({
    int? malId,
    String? name,
    String? url,
    int? count,
  }) {
    _malId = malId;
    _name = name;
    _url = url;
    _count = count;
  }

  Anime.fromJson(dynamic json) {
    _malId = json['mal_id'];
    _name = json['name'];
    _url = json['url'];
    _count = json['count'];
  }

  int? _malId;
  String? _name;
  String? _url;
  int? _count;

  Anime copyWith({
    int? malId,
    String? name,
    String? url,
    int? count,
  }) =>
      Anime(
        malId: malId ?? _malId,
        name: name ?? _name,
        url: url ?? _url,
        count: count ?? _count,
      );

  int? get malId => _malId;

  String? get name => _name;

  String? get url => _url;

  int? get count => _count;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['mal_id'] = _malId;
    map['name'] = _name;
    map['url'] = _url;
    map['count'] = _count;
    return map;
  }

  static String create() {
    return 'CREATE TABLE if not exists $tableAnime ($columnId INTEGER PRIMARY KEY, $columnName TEXT, $columnUrl TEXT, $columnCount INTEGER)';
  }
}

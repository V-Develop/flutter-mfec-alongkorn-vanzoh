import 'package:flutter/material.dart';
import 'package:flutter_mvvm/view/screen/anime_list_screen.dart';
import 'package:flutter_mvvm/view/screen/set_user_name_screen.dart';
import 'package:flutter_mvvm/view_model/set_user_name_view_model.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final SetUserNameViewModel _setUserNameViewModel = SetUserNameViewModel();

  @override
  void initState() {
    super.initState();
    _setUserNameViewModel.getUserName();
    _setUserNameViewModel.outputUserName.listen((event) {
      if (event.isEmpty) {
        Future.delayed(const Duration(milliseconds: 3000), () {
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => const SetUserNameScreen()));
        });
      } else {
        Future.delayed(const Duration(milliseconds: 3000), () {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => const AnimeListScreen()));
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.red,
        width: double.infinity,
        height: double.infinity,
        child: const Center(
          child: Text(
            "Splash screen by Alongkorn Vanzoh",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0, // insert your font size here
            ),
          ),
        ),
      ),
    );
  }
}

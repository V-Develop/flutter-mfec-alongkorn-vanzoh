import 'package:flutter/material.dart';
import 'package:flutter_mvvm/view/item/anime_card.dart';
import 'package:flutter_mvvm/view_model/anime_view_model.dart';

import '../../model/anime.dart';

class FavoriteAnimeListScreen extends StatefulWidget {
  const FavoriteAnimeListScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteAnimeListScreen> createState() =>
      _FavoriteAnimeListScreenState();
}

class _FavoriteAnimeListScreenState extends State<FavoriteAnimeListScreen> {
  final AnimeViewModel _animeViewModel = AnimeViewModel();

  @override
  void initState() {
    super.initState();
    _animeViewModel.fetchAnimeListFromDb();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('anime favorite'),
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Icon(Icons.arrow_back)),
      ),
      body: StreamBuilder(
        stream: _animeViewModel.outputAnimeListFromDb,
        builder: (BuildContext context, AsyncSnapshot<List<Anime>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data!.isEmpty) {
              return const Center(
                child: Text('no data'),
              );
            } else {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                  itemBuilder: (BuildContext context, int index) {
                return AnimeCard(anime: snapshot.data![index], isIcon: false);
              });
            }
          } else {
            return Container();
          }
        },
      ),
    );
  }
}

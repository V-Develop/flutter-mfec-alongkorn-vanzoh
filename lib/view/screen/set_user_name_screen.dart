import 'package:flutter/material.dart';
import 'package:flutter_mvvm/view/screen/anime_list_screen.dart';
import 'package:flutter_mvvm/view_model/set_user_name_view_model.dart';

class SetUserNameScreen extends StatefulWidget {
  const SetUserNameScreen({Key? key}) : super(key: key);

  @override
  State<SetUserNameScreen> createState() => _SetUserNameScreenState();
}

class _SetUserNameScreenState extends State<SetUserNameScreen> {
  final _controller = TextEditingController();
  String _name = '';
  final SetUserNameViewModel _setUserNameViewModel = SetUserNameViewModel();

  @override
  void initState() {
    super.initState();
    _setUserNameViewModel.outputUserName.listen((event) {
      if (event.isNotEmpty) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const AnimeListScreen()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              controller: _controller,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: const InputDecoration(
                labelText: 'Enter your name',
              ),
              validator: (text) {
                if (text == null || text.isEmpty) {
                  return 'Can\'t be empty';
                }
                if (text.length < 4) {
                  return 'Too short';
                }
                return null;
              },
              onChanged: (text) => setState(() => _name = text),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: _name.isNotEmpty ? _submit : null,
                child: Text(
                  'Submit',
                  style: Theme.of(context).textTheme.headline6,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _submit() {
    _setUserNameViewModel.setUserName(_name);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

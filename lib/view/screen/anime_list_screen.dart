import 'package:flutter/material.dart';
import 'package:flutter_mvvm/data/network/api_response.dart';
import 'package:flutter_mvvm/model/anime.dart';
import 'package:flutter_mvvm/model/anime_response.dart';
import 'package:flutter_mvvm/view/item/anime_card.dart';
import 'package:flutter_mvvm/view_model/anime_view_model.dart';
import 'package:flutter_mvvm/view_model/set_user_name_view_model.dart';

import 'favorite_anime_list_screen.dart';
import 'set_user_name_screen.dart';

class AnimeListScreen extends StatefulWidget {
  const AnimeListScreen({Key? key}) : super(key: key);
  

  @override
  State<AnimeListScreen> createState() => _AnimeListScreenState();
}

class _AnimeListScreenState extends State<AnimeListScreen> {
  final AnimeViewModel _animeViewModel = AnimeViewModel();
  final SetUserNameViewModel _setUserNameViewModel = SetUserNameViewModel();

  @override
  void initState() {
    super.initState();
    _animeViewModel.fetchAnimeListFromApi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Anime List'),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const FavoriteAnimeListScreen()));
            },
            child: const Icon(Icons.favorite),
          ),
          GestureDetector(
            onTap: () {
              _logout();
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const SetUserNameScreen()));
            },
            child: const Padding(
                padding: EdgeInsets.all(16.0), child: Icon(Icons.logout)),
          )
        ],
      ),
      body: StreamBuilder(
          stream: _animeViewModel.outputAnimeListFromApi,
          builder: (BuildContext context,
              AsyncSnapshot<ApiResponse<AnimeResponse>> snapshot) {
            switch (snapshot.data?.status) {
              case Status.LOADING:
                return const Center(
                  child: CircularProgressIndicator(),
                );
              case Status.ERROR:
                return Container();
              case Status.COMPLETED:
                List<Anime>? anime = snapshot.data?.data?.data ?? [];
                return ListView.builder(
                    itemCount: anime.length,
                    itemBuilder: (BuildContext context, int index) {
                      return AnimeCard(anime: anime[index], isIcon: true);
                    });
              default:
            }
            return Container();
          }),
    );
  }

  void _logout() {
    _setUserNameViewModel.removeUsername();
  }
}

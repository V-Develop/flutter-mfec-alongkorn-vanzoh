import 'package:flutter/material.dart';
import 'package:flutter_mvvm/view_model/anime_view_model.dart';

import '../../model/anime.dart';

class AnimeCard extends StatelessWidget {
  final Anime anime;
  final bool isIcon;

  AnimeCard({required this.anime, required this.isIcon, Key? key})
      : super(key: key);

  final AnimeViewModel animeViewModel = AnimeViewModel();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(child: Text(anime.name ?? '')),
          isIcon == true
              ? GestureDetector(
                  onTap: () {
                    animeViewModel.addAnimeListToApi(anime);
                  },
                  child: const Icon(Icons.favorite),
                )
              : Container(),
        ],
      ),
    );
  }
}

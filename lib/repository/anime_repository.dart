import 'package:flutter_mvvm/data/local/db_config.dart';
import 'package:flutter_mvvm/data/network/api_end_points.dart';
import 'package:flutter_mvvm/data/network/base_api_service.dart';
import 'package:flutter_mvvm/data/network/network_api_service.dart';
import 'package:flutter_mvvm/model/anime.dart';
import 'package:flutter_mvvm/model/anime_response.dart';

class AnimeRepository {
  final BaseApiService _apiService = NetworkApiService();
  final DbConfig _db = DbConfig();

  Future<AnimeResponse> getAnimeListFromApi() async {
    dynamic response = await _apiService.getResponse(ApiEndPoints().getAnime);
    final jsonData = AnimeResponse.fromJson(response);
    return jsonData;
  }

  Future<List<Anime>> getAnimeListFromDb() async {
    var db = await _db.getInstance();
    List<Map<String, Object?>>? maps = await db?.query(tableAnime);
    List<Anime> response = [];
    maps?.forEach((element) {
      response.add(Anime.fromJson(element));
    });
    return response;
  }

  Future<void> insertAnimeToDb(Anime anime) async {
    var db = await _db.getInstance();
    await db?.insert(tableAnime, anime.toJson());
  }
}

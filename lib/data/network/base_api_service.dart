abstract class BaseApiService {

  final String baseUrl = "https://api.jikan.moe/v4/";

  Future<dynamic> getResponse(String url);
  Future<dynamic> postResponse(String url,Map<String, String> jsonBody);

}
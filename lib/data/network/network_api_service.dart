import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'base_api_service.dart';

class NetworkApiService extends BaseApiService {
  @override
  Future getResponse(String url) async {
    dynamic responseJson;
    final response = await http.get(Uri.parse(baseUrl + url));
    responseJson = returnResponse(response);
    return responseJson;
  }

  @override
  Future postResponse(String url, Map<String, String> JsonBody) async {
    dynamic responseJson;
    final response = await http.post(Uri.parse(baseUrl + url), body: JsonBody);
    responseJson = returnResponse(response);
    return responseJson;
  }

  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        return;
      case 401:
      case 403:
        return;
      case 404:
        return;
      case 500:
      default:
        return;
    }
  }
}

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../../model/anime.dart';

class DbConfig {
  Future<Database?> getInstance() async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, 'database.db');

    return await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      db.execute(Anime.create());
    });
  }
}
